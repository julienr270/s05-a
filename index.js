// Mathematical Operations (-, *, /, %)
// Subtraction 
let numString1 = "5";
let numString2 = "6";
let num1 = 5;
let num2 = 6;
let num3 = 5.5;
let num4 = .5;

console.log(num1-num3); //-0.5 results in proper mathematical operation
console.log(num3-num4); //5 results in proper mathematical operation
console.log(numString1-num2); //-1 results in proper mathematical operation because the string was forced to become a number
console.log(numString2-num2); //0 results in proper mathematical operation because the string was forced to become a number
console.log(numString1-numString2); //-1 in subtraction, numeric strings will not concatenate and instead will be forceibly changed its type and subtract properly

let sample2 = "Juan Dela Cruz";
console.log(sample2 - numString1); // NaN - results in Not in Number. When trying to perform subtraction between alphanumeric string and numeric string, the result is NaN.

// Multiplication
console.log(num1*num2);//30
console.log(numString1*num1); //25
console.log(numString1*numString2);//30
console.log(sample2 * 5); //Nan

let product1 = num1 * num2;
let product2 = numString1 * num1;
let product3 = numString1 * numString2;
console.log(product1); //30

//Division
console.log(product1/num2);//5
console.log(product2/5); //5
console.log(numString2 / numString1); //1.2
console.log(numString2 % numString1); //1

//division/multiplication by 0
console.log(product2 * 0); //0
console.log(product3 / 0); //0
// Division by 0 is not accurately and should not be done because it results to infinity


// % Modulo - remainder of division operation
console.log(product2 % num2); //product2/num2 (25/6) = remainder - 1
console.log(product3 % product2); //30/25 = remainder - 5
console.log(num1 % num2); //5
console.log(num1 % num1); //0

// Boolean (true or false)
/*
	Boolean is usually used for logic operations or if-else condition
		- boolean values are normally used to store values relating to the state of a certain things
	When creating a variable which will contain a boolean, the variable name is usually a yes or no question
*/

let isAdmin = true;
let isMarried = false;
let isMVP = true;

// you can also concatenate strings boolean 
console.log("Is she married? " + isMarried);
console.log("Is he the MVP? " + isMVP);
console.log(`Is he the current Admin? ` + isAdmin);

// Arrays
/*
	Arrays 
	- are a special kind of data type to store multiple values
	- can actually store data with different types BUT as the best practice, arrays are used to contain multiple values of the SAME data type
	- values in array are separated by commas
	An array is created with an array literal = []
*/
// Syntax:
	// let/const arrayName = [elementA, elementB, elementC, ...];
let array1 = ["Goku", "Piccolo", "Gohan", "Vegeta"];
console.log(array1);
let array2 = ["One Punch Man", true, 500, "Saitama" ];
console.log(array2);

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Array are better thought of as group of data

//Objects
/*
	Objects
		- are another special kind of data type used to mimic the real world
		- used to create complex data that contain pieces of information that are relevant to each other
		- Objects are created with object literals = {}
			each data/valuee are paired with a key
			-each field is called a property
			-each field is separated by comma

			Syntax:
				let/const objctName = {
					propertyA: value,
					propertyB: value
				}
*/
let person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact: ["+639154847451", "8123 4567"],
	address: {
		houseNumber: '345',
		street: 'Diamond',
		city: 'Manila',
	}
};

console.log(person);

/*Mini-Activity
	Create a variable with a group of data
		The group of data should contain names from your favorite band/idol

	Create a variable which contain multiple values of differing types and describes a single person.
		-This data type should be able to contain multiple key value pairs.
			firstName: <value>
			lastName: <value>
			isDeveloper: <value>
			hasPortfolio: <value>
			age: <value>
			contact: <value>
			adress: 
				houseNumber: <value>
				street: <value>
				city: <value>

*/

let idols = ["Momo", "Sana", "Mina", "Chaeyoung"];
let self = {
	fullName: 'Julien',
	lastName: 'Rosales',
	isDeveloper: false,
	hasPortfolio: false,
	age: 35,
	contact: ["+639174206996", "8954 2212"],
	address: {
		houseNumber: '012',
		street: 'Sampaguita',
		city: 'Mandaluyong',
	}
};
console.log(idols);
console.log(self);

// Undefined vs Null
	// Null - is explicit absence of data/value. This is done to project that a variable contains nothing over undefined as undefined merely means there is no data in the variable BECAUSE the variable has not been assigned an initial value

let sampleNull = null;

// Undefined is a represenation that a variable has been declared but it was not assigned an initial value

let sampleUndefined;

console.log(sampleNull);
console.log(sampleUndefined);

// certain processes in programming explicitly return null to indicate that the task resulted to nothing

let foundResult = null;

//Example:
let myNumber = 0;
let myString = "";
// using null to compare to a 0 value and an empty string  is much better for readability

// for undefined, this is normally caused by developers creating variables that have no value or data/associated with them
//This is when a variable does not exist but its value is still unknown.

let person2 = {
	name: "Peter",
	age: 35
}
// because person2 does exist but the property isAdmin does not exist.
console.log(person2.isAdmin);

/* [SECTION] Functions
	Functions
		- in JavaScript are lines/block of codes that tell our device/application to perform a certain task when called/invoke
		- are reusable pieces of code with instructions which usd over and over again just as long as we can call/invoke them.

		Syntax:
			function functionName(){
				code block
					- the block of codes that will be executed once the function has been ran/called/invoked
			}

*/

//Declaring Function
function printName1(){
	console.log("My name is Sana");
};

// invoking/calling of function
printName1();
printName1();
printName1();

function showSum(){
	console.log(25+6);
};

showSum();
// Note: do not create functions with the same name

/*Parameters and Arguments
	"name" is called a parameter
		a parameter acts a named variable/container that exists ONLY inside og the function. This is used as to store information/ to act as a stand-in or the containe the value passed into the function as an argument	
*/

function printName(name){
	console.log(`My name is ${name}`);
};

//When a function is invoked and data is passed, we called the data as argument
//In this invokation, "Momo" is an argument passed into our m,ain function and is represented by the "name" parameter within our function.
// data passed into the function: argument
// representation of the argument within the function: parameter
printName("Momo");

function displayNum(number){
	alert(number);
};

displayNum(1000);

/*Mini Activity
	Create a function which will be able to show message in the console.
		The message should be able to be passed into function via an argument.

		- Sample Message:
			"JavaScript is Fun"

			argument; programming language
*/

function printFun(programmingLanguage){
	console.log(`${programmingLanguage} is fun`);
};
printFun("JavaScript");
printFun("C++");

//Multiple parameters and arguments

function displayFullName(firstName, mi, lastName, age){
	console.log(`${firstName} ${mi} ${lastName} ${age}`);
};

displayFullName("Janyn", "D", "Baisas", 21);

// return keyword
	// The return statement allows the output of a function to be passed to the line/block of code that invoked/called the function
	//any line/block of code that comes after the return statemtn is ignored because it ends the function execution

function createFullName(firstName, middleName, lastName){
	// return keywrod is used so that a function may return a value
	// it also stops the process of the function or any other instruction after the keyword will not be processed
	return `${firstName} ${middleName} ${lastName}`
	console.log("I will no longer run because the function value/result has been returned.");
};

let fullName1 = createFullName("Sana", "TWICE", "Minatozaki");
let fullName2 = displayFullName("Tom", "Mapother", "Cruz");
let fullName3 = createFullName("Momo", "TWICE", "Hirai");

console.log(fullName1);
console.log(fullName2);
console.log(fullName3);